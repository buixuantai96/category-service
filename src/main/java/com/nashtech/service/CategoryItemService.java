package com.nashtech.service;

import com.nashtech.domain.CategoryItem;
import com.nashtech.repository.CategoryItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CategoryItem.
 */
@Service
@Transactional
public class CategoryItemService {

    private final Logger log = LoggerFactory.getLogger(CategoryItemService.class);

    private final CategoryItemRepository categoryItemRepository;

    public CategoryItemService(CategoryItemRepository categoryItemRepository) {
        this.categoryItemRepository = categoryItemRepository;
    }

    /**
     * Save a categoryItem.
     *
     * @param categoryItem the entity to save
     * @return the persisted entity
     */
    public CategoryItem save(CategoryItem categoryItem) {
        log.debug("Request to save CategoryItem : {}", categoryItem);
        return categoryItemRepository.save(categoryItem);
    }

    /**
     * Get all the categoryItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CategoryItem> findAll(Pageable pageable) {
        log.debug("Request to get all CategoryItems");
        return categoryItemRepository.findAll(pageable);
    }


    /**
     * Get one categoryItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CategoryItem> findOne(Long id) {
        log.debug("Request to get CategoryItem : {}", id);
        return categoryItemRepository.findById(id);
    }

    /**
     * Delete the categoryItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CategoryItem : {}", id);
        categoryItemRepository.deleteById(id);
    }
}
